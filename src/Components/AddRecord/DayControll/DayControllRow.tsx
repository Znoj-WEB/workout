import React from "react";

import { get2diffColumnsRow } from "../../../utils/row";
import DayControllComponent from "./DayControllComponent";

function DayControllRow() {
  const title: string = "Day";
  const inputComponent: React.ReactElement = <DayControllComponent />;
  return <>{get2diffColumnsRow(title, inputComponent)}</>;
}

export default DayControllRow;
